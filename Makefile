DOCKER_IMAGE_VERSION=latest
DOCKER_IMAGE_NAME=amaelh/rpi-openvpn
DOCKER_IMAGE_TAGNAME=$(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_VERSION)

default: push

refresh:
		docker pull alpine:latest

build: refresh
		docker build -t $(DOCKER_IMAGE_TAGNAME) .
		docker tag $(DOCKER_IMAGE_TAGNAME) $(DOCKER_IMAGE_NAME):latest

test: build
		docker run --rm $(DOCKER_IMAGE_TAGNAME) /bin/echo "Success."

push: test
		docker push $(DOCKER_IMAGE_NAME)

version:
		docker run --rm $(DOCKER_IMAGE_TAGNAME) uname -a

